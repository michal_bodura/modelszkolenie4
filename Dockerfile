FROM python:3.9-alpine

ENV PYTHONBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /home/user/Projekt/szkolenie

COPY requirements.txt requirements.txt
RUN mkdir -p /usr/src/app/restful
RUN apk update && apk add && \
    apk add --no-cache --update libpq && \
    apk add --no-cache --update gettext libintl && \
    apk add --no-cache --update postgresql-libs gcc libc-dev && \
    apk add --no-cache --virtual .build-deps postgresql-dev py3-psycopg2 py3-pillow gcc python3-dev musl-dev jpeg-dev zlib-dev gettext-dev libjpeg && \
    apk add --no-cache --virtual .pynacl_deps build-base python3-dev libffi-dev && \
    python3 -m pip install -r requirements.txt && \
    apk --purge del .build-deps 


# copy entrypoint.sh
COPY ./entrypoint.sh .
RUN sed -i 's/\r$//g' /home/user/Projekt/szkolenie/entrypoint.sh
RUN chmod +x /home/user/Projekt/szkolenie/entrypoint.sh

# copy project
COPY . .

# run entrypoint.sh
ENTRYPOINT ["/home/user/Projekt/szkolenie/entrypoint.sh"]

