"""szkolenie URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.http import HttpResponse
from django.urls import path
from modelszkolenie import views
from modelszkolenie.admin import my_admin_site

app_name = 'modelszkolenie'


def okay(request):
    return HttpResponse('pretend-binary-data-here', content_type='image/jpeg')


urlpatterns = i18n_patterns(
    path('admin/', my_admin_site.urls),
    path('', views.home, name='home'),
    path('api/company', views.CompanyList.as_view()),
    path('api/company/<int:pk>', views.CompanyDetail.as_view()),
    path('api/user/', views.UserList.as_view()),
    path('api/user/<int:pk>', views.UserDetail.as_view()),
    path('api/training/', views.TrainingList.as_view()),
    path('api/training/<int:pk>', views.TrainingDetail.as_view()),
    path('api/question', views.QuestionList.as_view()),
    path('api/question/<int:pk>', views.QuestionDetail.as_view()),
    path('api/answer', views.AnswerList.as_view()),
    path('api/user/<int:pk>/trainings', views.TrainingUserList.as_view()),
    path('api/question/<int:pk>/answers', views.QuestionsAnswerList.as_view()),
    path('favicon.ico', okay),
    prefix_default_language=False
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
