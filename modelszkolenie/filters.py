from .models import *
from datetime import date
from django_property_filter.filters import PropertyDateFilter
from django_property_filter import PropertyNumberFilter, PropertyFilterSet
from django.contrib.admin import SimpleListFilter
from django.utils.translation import gettext_lazy as _

class NumAnswersSet(PropertyFilterSet):
    num_answers = PropertyNumberFilter(field_name='numOfAnswers', lookup_expr='exact')

    class Meta:
        model = Question
        fields = ['num_answers']

class NumGuestsSet(PropertyFilterSet):
    num_guests = PropertyNumberFilter(field_name='numOfGuests', lookup_expr='lte')

    class Meta:
        model = Training
        fields = ['num_guests']

class NumQuestionsSet(PropertyFilterSet):
    num_questions = PropertyNumberFilter(field_name='numOfQuestions', lookup_expr='lte')

    class Meta:
        model = Training
        fields = ['num_questions']

class ExpDateSet(PropertyFilterSet):
    unexp_date = PropertyDateFilter(field_name='expiration_date', lookup_expr='lte')
    exp_date = PropertyDateFilter(field_name='expiration_date', lookup_expr='gt')
    class Meta:
        model = CompletedTraining
        fields = ['unexp_date','exp_date']

class TrainingTimeFilter(SimpleListFilter):
    title = _('Time')
    parameter_name = 'time'
     
    def lookups(self, request,model_admin):
        return [
            ('1-7', _('week')),
            ('1-30', _('month')),
            ('1-90',_('quartile')),
            ('1-365',_('year'))
    ]
    
    def queryset(self, request, queryset):

        if (self.value() == '1-7'):
            return queryset.filter(time__range=[1,7])
        if (self.value() == '1-30'):
            return queryset.filter(time__range=[1,30])
        if (self.value() == '1-90'):
            return queryset.filter(time__range=[1,90])
        if (self.value() == '1-365'):
            return queryset.filter(time__range=[1,365])

class TrainingObligatoryFilter(SimpleListFilter):
    title = _('status')
    parameter_name = 'obligatory'
    def lookups(self, request,model_admin):
        return [
            ('obligatory', _('obligatory')),
            ('unobligatory', _('unobligatory'))
    ]

    def queryset(self, request, queryset):
    
        if (self.value() == 'obligatory'):
            return queryset.filter(obligatory=True)
        if (self.value() == 'unobligatory'):
            return queryset.filter(obligatory=False)

class QuestionsAnswersSetFilter(SimpleListFilter):
    title = _('Status')
    parameter_name = 'status'
    def lookups(self, request,model_admin):
        return [
            ('correct', _('correct')),
            ('uncorrect', _('uncorrect'))
    ]

    def queryset(self, request, queryset):
    
        if (self.value() == 'correct'):
            return queryset.filter(status=True)
        if (self.value() == 'uncorrect'):
            return queryset.filter(status=False)

class NumAnswersFilter(SimpleListFilter):
    title = _("Number of answers")
    parameter_name = 'numOfAnswers'
    def lookups(self, request,model_admin):
        return [
            ('1', _('one')),
            ('2', _('two')),
            ('3', _('three')),
            ('4', _('four')),
            ('5', _('five')),
    ]
    def queryset(self, request, queryset):
        if (self.value() == '1'):
            return queryset.filter(answers__count=1)
        if (self.value() == '2'):
            return queryset.filter(answers__count=2)
        if (self.value() == '3'):
            return queryset.filter(answers__count=3)
        if (self.value() == '4'):
            return queryset.filter(answers__count=4)
        if (self.value() == '5'):
            return queryset.filter(answers__count=5)

class NumQuestionsFilter(SimpleListFilter):
    title = _("Number of questions")
    parameter_name = 'numOfQuestions'
    def lookups(self, request,model_admin):
        return [
            ('<= 5', '<=5'),
            ('6-10', '6-10'),
            ('11-15', '11-15'),
            ('16-20', '16-20'),
            ('> 20', '> 20'),
    ]
    def queryset(self, request, queryset):
        if (self.value() == '<= 5'):
            return queryset.filter(questions__count__lte=5)
        if (self.value() == '6-10'):
            return queryset.filter(questions__count__range=[6,10])
        if (self.value() == '11-15'):
            return queryset.filter(questions__count__range=[11,15])
        if (self.value() == '16-20'):
            return queryset.filter(questions__count__range=[16,20])
        if (self.value() == '> 20'):
            return queryset.filter(questions__count__gt=20)

class NumGuestsFilter(SimpleListFilter):
    title = _("Number of guests")
    parameter_name = 'numOfGuests'
    def lookups(self, request,model_admin):
        return [
            ('<= 10', '<= 10'),
            ('11-20', '11-20'),
            ('21-35', '21-35'),
            ('36-50', '36-50'),
            ('> 50', '> 50'),
    ]
    def queryset(self, request, queryset):
        if (self.value() == '<= 10'):
            return queryset.filter(questions__count__lte=10)
        if (self.value() == '11-20'):
            return queryset.filter(questions__count__range=[11,20])
        if (self.value() == '21-35'):
            return queryset.filter(questions__count__range=[21,35])
        if (self.value() == '36-50'):
            return queryset.filter(questions__count__range=[36,50])
        if (self.value() == '> 50'):
            return queryset.filter(questions__count__gt=50)

class ExpDateFilter(SimpleListFilter):
    title = _("Expirated")
    parameter_name = "expiration_date"
    def lookups(self, request,model_admin):
        return [
            ('Unexpired',_('Unexpired')),
            ('Expired',_('Expired')),
        
    ]
    def queryset(self, request, queryset):
        if (self.value() == 'Unexpired'):
            return queryset.filter(completed_date__gte=date.today()-F("training__time"))
        if (self.value() == 'Expired'):
            return queryset.filter(completed_date__lt=date.today()-F("training__time"))