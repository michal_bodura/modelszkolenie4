from .filters import *
from .forms import *
from .models import *
from .resources import *
from admin_confirm import AdminConfirmMixin
from django.contrib.admin import AdminSite, ModelAdmin, TabularInline, action as Action, HORIZONTAL, display as Display
from django.contrib.auth.models import User as AuthUser, Group
from django.contrib.messages import SUCCESS, success, error
from django.utils.translation import ngettext, gettext_lazy as _
from easy import short as Description
from import_export.admin import ExportMixin, ImportExportMixin, ImportExportModelAdmin # Import biblioteki umożliwiającej eksportowanie i importowanie danych z/do panelu administracyjnego

# Ustawienie stron tytułowych w panelu administracyjnym

class MyAdminSite(AdminSite):
    index_title = _('Welcome')
    site_header = _('Admin panel')
    site_title = _('Admin panel')
    empty_value_display = "---"

    def get_app_list(self, request):
        """
        Return a sorted list of all the installed apps that have been
        registered in this site.
        """
        ordering = {
            "Goście": 1,
            "Firmy": 2,
            "Szkolenia": 3,
            "Pytania": 4,
            "Odpowiedzi": 5,
            "Ukończone szkolenia": 6,
            "Galeria obrazów": 7,
            "Obrazy do pytań": 8,
            "Obrazy do szkoleń": 9,
            "Użytkownicy": 10,
            "Grupy": 11,
        }
        app_dict = self._build_app_dict(request)
        # a.sort(key=lambda x: b.index(x[0]))
        # Sort the apps alphabetically.
        app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())

        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: ordering[x['name']])

        return app_list


# Inicjacja klasy MyAdminSite z widoku panelu administracyjnego
    
my_admin_site = MyAdminSite(name='admin')

# Wykonanie akcji updatowych dla klas w panelu administracyjnym dla pola status

@Action(description=_('Change to good'))
def mark_correct(self, request, queryset):
    updated = queryset.update(status=True)
    self.message_user(request, ngettext(
        '%d ' + _('answer changed to correct.'),
        '%d ' + _('answers changed to correct.'),
        updated,
    ) % updated, SUCCESS)


@Action(description=_('Change to wrong'))
def mark_uncorrect(self, request, queryset):
    updated = queryset.update(status=False)
    self.message_user(request, ngettext(
        '%d ' + _('answer changed to uncorrect.'),
        '%d ' + _('answers changed to uncorrect.'),
        updated,
    ) % updated, SUCCESS)

# Wykonanie akcji updatowych dla klas w panelu administracyjnym dla pola obligatory

@Action(description=_('Change to obligatory'))
def mark_obligatory(self, request, queryset):
    updated = queryset.update(obligatory=True)
    polish_plural_expr = ((updated == 1) or ((updated % 10 >= 2 and updated % 10 <= 4))) and (int(updated/10) != 1)
    if (polish_plural_expr):
        return self.message_user(request, ngettext(
            '%d ' + _('training changed to obligatory.'),
            '%d ' + _('trainings changed to obligatory.'),
            updated,
        ) % updated, SUCCESS)
    else:
        return self.message_user(request, ngettext(
            '%d ' + _('training changed to obligatory.'),
            '%d ' + _('trainings pro changed to obligatory.'),
            updated,
        ) % updated, SUCCESS)

@Action(description=_('Change to unobligatory'))
def mark_unobligatory(self, request, queryset):
    updated = queryset.update(obligatory=False)
    polish_plural_expr = ((updated == 1) or ((updated % 10 >= 2 and updated % 10 <= 4))) and (int(updated/10) != 1)
    if (polish_plural_expr):
        self.message_user(request, ngettext(
            '%d ' + _('training changed to unobligatory.'),
            '%d ' + _('trainings changed to unobligatory.'),
            updated,
        ) % updated, SUCCESS)
    else:
        self.message_user(request, ngettext(
            '%d ' + _('training changed to unobligatory.'),
            '%d ' + _('trainings pro changed to unobligatory.'),
            updated,
        ) % updated, SUCCESS)
        
# Wykonanie akcji updatowych dla klas w panelu administracyjnym dla pola language

@Action(description=_('Change to english'))
def set_english(self, request, queryset):
    updated = queryset.update(language='EN')
    if (((updated == 1) or ((updated % 10 >= 2 and updated % 10 <= 4))) and (int(updated/10) != 1)):
        self.message_user(request, ngettext(
            '%d ' + _('record changed to english.'),
            '%d ' + _('records changed to english.'),
            updated,
        ) % updated, SUCCESS)
    else:
        self.message_user(request, ngettext(
            '%d ' + _('record changed to english.'),
            '%d ' + _('records pro changed to english.'),
            updated,
        ) % updated, SUCCESS)

@Action(description=_('Change to polish'))
def set_polish(self, request, queryset):
    updated = queryset.update(language='PL')
    if (((updated == 1) or ((updated % 10 >= 2 and updated % 10 <= 4))) and (int(updated/10) != 1)):
        self.message_user(request, ngettext(
            '%d ' + _('record changed to polish.'),
            '%d ' + _('records changed to polish.'),
            updated,
        ) % updated, SUCCESS)
    else:
        self.message_user(request, ngettext(
            '%d ' + _('record changed to polish.'),
            '%d ' + _('records pro changed to polish.'),
            updated,
        ) % updated, SUCCESS)

# Klasa umożliwiająca wyświetlanie "inline" w modelu Question danych z klasy QuestionsAnswersSet w sposób tabelaryczny w panelu administracyjnym

class QuestionAnswersSetInline(TabularInline):
    model = Question.answers.through
    verbose_name = _("Q&A set")
    extra = 1
    min_num = 1
    max_num = 5
    classes = ['collapse', 'show']

class QuestionTrainingSetInline(TabularInline):
    model = TrainingQuestionProxy
    verbose_name = _("QuestionSet")
    verbose_name_plural = _("List of questions")
    extra = 1
    classes = ['collapse', 'show']

class GaleryImageTrainingSetInline(TabularInline):
    model = TrainingGalleryProxy
    verbose_name = _("Gallery images")
    verbose_name_plural = _("Gallery")
    extra = 1
    classes = ['collapse', 'show']
    
# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Question

class QuestionAdmin(AdminConfirmMixin,ImportExportModelAdmin, ModelAdmin):

    confirm_add = True
    confirm_change = True
    confirmation_fields = ['content','numOfAnswers','language',"sequence"]
    action_callback = True
    resource_class = QuestionResource
    list_display = ['content','numOfAnswers','language',"sequence"]
    list_filter = [NumAnswersFilter,'language']
    search_fields = ['content','language','answers__answer' ]
    list_display_links = ['content']
    list_per_page = 20
    actions=[set_english,set_polish]
    radio_fields = {"language": HORIZONTAL}
    fieldsets = [
        (_('Question content'), {'fields': ['content','language','image','sequence']}),
    ]

    inlines = [
        QuestionAnswersSetInline
    ]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(Count('answers'))
        return qs

    @Description(short_description=_('NumOfAnswers'),admin_order_field="answers__count")
    @Display(empty_value="---")
    def numOfAnswers(self, obj: Question) -> str:
        return obj.numOfAnswers

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionsFile"),
                    file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.content == form.cleaned_data['content']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed question sucessfully"))
                else:
                    error(request, _("Changed question unsucessfully"))
            else:
                if obj.content == form.cleaned_data['content']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added question sucessfully"))
                else:
                    error(request, _("Added question unsucessfully"))

    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted question sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('question was deleted sucessfully.'),
                '%d ' + _('questions were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('question was deleted sucessfully.'),
                    '%d ' + _('questions pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Training

class TrainingAdmin(AdminConfirmMixin,ImportExportModelAdmin, ModelAdmin):

    action_callback = True
    confirm_add = True
    confirm_change = True
    confirmation_fields = ['name','time_str','obligatory','numOfGuests','numOfQuestions','language']
    resource_class = TrainingResource
    list_filter = ['language',TrainingObligatoryFilter,TrainingTimeFilter,NumQuestionsFilter, NumGuestsFilter]
    list_display = ['name','time_str','obligatory','numOfGuests','numOfQuestions','language']
    search_fields = ['name','time','language']
    list_display_links = ['name']
    list_per_page = 20
    radio_fields = {"language": HORIZONTAL}
    actions = [mark_obligatory, mark_unobligatory,set_polish,set_english]
    fieldsets = [
        (_('Training data'), {'fields': ['name','time','obligatory','image','language']}),
    ]
    
    inlines = [
        GaleryImageTrainingSetInline,
        QuestionTrainingSetInline,
        
    ]
  
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(Count('users'))
        qs = qs.annotate(Count('questions'))
        return qs
    
    @Description(short_description=_('ActiveDuration'),admin_order_field='time')
    @Display(empty_value="---")
    def time_str(self, obj: Training) -> str:
        return obj.time_str
    
    @Description(short_description=_('NumOfGuests'),admin_order_field="users__count")
    @Display(empty_value="---")
    def numOfGuests(self, obj: Training) -> str:
        return obj.numOfGuests
    
    @Description(short_description=_('NumOfQuestions'),admin_order_field="questions__count")
    @Display(empty_value="---")
    def numOfQuestions(self, obj: Training) -> str:
        return obj.numOfQuestions

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("TrainingsFile"),
                    file_format.get_extension())
        return filename
    
    def get_ordering(self,request):
        return ['-time']
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed training sucessfully"))
                else:
                    error(request, _("Changed training unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added training sucessfully"))
                else:
                    error(request, _("Added training unsucessfully"))
                    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted training sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('training was deleted sucessfully.'),
                '%d ' + _('trainings were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('training was deleted sucessfully.'),
                    '%d ' + _('trainings pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Answer

class AnswerAdmin(AdminConfirmMixin,ImportExportModelAdmin, ModelAdmin):

    action_callback = True
    confirm_add = True
    confirm_change = True
    confirmation_fields = ['answer']
    resource_class = AnswerResource
    fields = ['answer']
    list_display = ['answer']
    list_per_page = 20
    search_fields = ['answer']

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("AnswersFile"),
                          file_format.get_extension())
        return filename

    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions
    
    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.answer == form.cleaned_data['answer']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed answer sucessfully"))
                else:
                    error(request, _("Changed answer unsucessfully"))
            else:
                if obj.answer == form.cleaned_data['answer']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added answer sucessfully"))
                else:
                    error(request, _("Added answer unsucessfully"))
        
    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted answer sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            success(request, ngettext(
                '%d ' + _('answer was deleted sucessfully.'),
                '%d ' + _('answers were deleted sucessfully.'),
                record_num,
            ) % record_num)

# Klasa umożliwiająca wyświetlanie danych dotyczących modelu Company

class CompanyAdmin(AdminConfirmMixin,ImportExportMixin, ModelAdmin):
    action_callback = True
    confirm_add = True
    confirm_change = True
    confirmation_fields = ['name','real_address']
    resource_class = CompanyResource
    fieldsets = [
        (_('Company data'), {'fields': ['name','address','city']})
    ]
    list_display = ['name','real_address']
    search_fields = ['name','address','city']
    list_display_links = ['name']
    list_filter = ['city']
    list_per_page = 20

    @Description(short_description=_('real_address'),admin_order_field='address')
    @Display(empty_value="---")
    def real_address(self, obj: Company) -> str:
        return obj.real_address

    def get_ordering(self,request):
        return ['name']

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("CompaniesFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed company sucessfully"))
                else:
                    error(request, _("Changed company unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added company sucessfully"))
                else:
                    error(request, _("Added company unsucessfully"))
    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted company sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('company was deleted sucessfully.'),
                '%d ' + _('companies were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('company was deleted sucessfully.'),
                    '%d ' + _('companies pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)
    
# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu User

class UserAdmin(ImportExportModelAdmin, ModelAdmin):

    action_callback = True
    resource_class = UserResource
    add_form = [CustomUserCreationForm]
    form = CustomUserChangeForm
    list_display = ['first_name','last_name','company','email','id_card','country']
    list_display_links = ['email','id_card']
    list_filter = ['company__city','company__name']
    search_fields = ['email','id_card','country','company__name','company__city']
    list_per_page = 25

    fieldsets = [
        (_('User data'), {'fields': [('first_name','last_name'),('email','id_card')]}),
        (_('Additional data'), {'fields': ['company','country']}),
    ]
    
    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("GuestsFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions

    def message_user(self,request, *args):
        if self.action_callback:
            success(request,args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.id_card == form.cleaned_data['id_card']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed guest sucessfully"))
                else:
                    error(request, _("Changed guest unsucessfully"))
            else:
                if obj.id_card == form.cleaned_data['id_card']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added guest sucessfully"))
                else:
                    error(request, _("Added guest unsucessfully"))

    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted guest sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            success(request, ngettext(
                '%d ' + _('guest was deleted sucessfully.'),
                '%d ' + _('guests were deleted sucessfully.'),
                record_num,
            ) % record_num)
    
# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu CompletedTraining

class CompletedTrainingAdmin(AdminConfirmMixin,ExportMixin, ModelAdmin):
    action_callback = True
    confirm_add = True
    confirm_change = True
    confirmation_fields = ['guest', 'expiration_date','completed_date', 'training']
    date_hierarchy = 'completed_date'
    date_hierarchy_drilldown = False
    resource_class = CompletedTrainingResource
    readonly_fields = ["expiration_date"]
    list_display = ['guest', 'expiration_date','completed_date', 'training']
    list_display_links = ['guest']
    list_filter = ['completed_date','guest__company__name','guest',ExpDateFilter]
    list_per_page = 20
    list_select_related = ('guest','training')
    
    search_fields = ['guest__email','guest__id_card','guest__company__name', 'training__name']
    fieldsets = [
        (_('Personlal date'), {'fields': ['guest','training']}),
        (_('Completed training'),{ 'classes': ('collapse', 'open'),'fields': ['completed_date','expiration_date']}),
        
    ]

    
    @Description(short_description=_('Expiration date'), help_text = _('Expiration date'), admin_order_field=F("completed_date")+F("training__time"))
    @Display(empty_value="---")
    def expiration_date(self, obj: CompletedTraining) -> str:
        return obj.expiration_date

    def get_ordering(self,request):
        return ['-completed_date']
            
    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("CompletedTrainingsFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions
    
    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.guest == form.cleaned_data['guest']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed completed training sucessfully"))
                else:
                    error(request, _("Changed completed training unsucessfully"))
            else:
                if obj.guest == form.cleaned_data['guest']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added completed training sucessfully"))
                else:
                    error(request, _("Added completed training unsucessfully"))
        
    
    def delete_model(self, request, obj):
        super().delete_model(request, obj)
        success(request, _("Deleted completed training sucessfully"))

    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('completed training was deleted sucessfully.'),
                '%d ' + _('completed trainings were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('completed training was deleted sucessfully.'),
                    '%d ' + _('completed trainings pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)
    
# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu QuestionsAnswersSet

class QuestionsAnswersSetAdmin(AdminConfirmMixin,ExportMixin, ModelAdmin):
    action_callback = True
    confirm_add = True
    confirm_change = True
    confirmation_fields = ['question','answer','status']
    resource_class = QuestionsAnswersSetResource
    list_display = ['question','answer','status']
    list_display_links = ['question','answer']
    list_filter = [QuestionsAnswersSetFilter,'question']
    actions = [mark_correct, mark_uncorrect]
    list_per_page = 20
    search_fields = ['question__content','answer__answer','status']
    fieldsets = [
        (_('Question state'), {'fields': ['question','answer','status']}),
    ]

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionsAnswersSetFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions
    
    def message_user(self, request,*args):
        if self.action_callback:
            success(request,args[0])

    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.question == form.cleaned_data['question']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed questions and answers sucessfully"))
                else:
                    error(request, _("Changed questions and answers unsucessfully"))
            else:
                if obj.question == form.cleaned_data['question']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added questions and answers sucessfully"))
                else:
                    error(request, _("Added questions and answers unsucessfully"))
        
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted questions and answers set sucessfully"))
        
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('questions and answers set was deleted sucessfully.'),
                '%d ' + _('questions and answers sets were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('questions and answers set deleted sucessfully.'),
                    '%d ' + _('questions and answers sets pro were deleted sucessfully.'),
                    record_num,
                )% record_num)
    

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu TrainingImage

class TrainingImageAdmin(ExportMixin, ModelAdmin):
    action_callback = True
    resource_class = TrainingImageResource
    date_hierarchy = 'date'
    date_hierarchy_drilldown = False
    list_filter = ['date']
    list_display = ["titleImage","date"]
    list_display_links = ['titleImage']
    list_per_page = 20
    search_fields = ['titleImage','date']

    fieldsets = [
        (_('Image data'), {'fields': ["titleImage","image"]}),
    ]

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("TrainingImagesFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions
    
    def message_user(self, request,*args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed training image sucessfully"))
                else:
                    error(request, _("Changed training image unsucessfully"))
            else:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added training image sucessfully"))
                else:
                    error(request, _("Added training image unsucessfully"))
    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted training image sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('training image was deleted sucessfully.'),
                '%d ' + _('training images were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('training image deleted sucessfully.'),
                    '%d ' + _('training images were deleted sucessfully.'),
                    record_num,
                ) % record_num)


# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu QuestionImage

class QuestionImageAdmin(ExportMixin, ModelAdmin):
    action_callback = True
    resource_class = QuestionImageResource
    date_hierarchy = 'date'
    date_hierarchy_drilldown = True
    list_display = ["titleImage","date"]
    list_filter = ['date']
    list_display_links = ['titleImage']
    search_fields = ['titleImage','date']
    list_per_page = 20

    fieldsets = [
        (_('Image data'), {'fields': ["titleImage","image"]}),
    ]

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("QuestionImagesFile"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        return actions
    
    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed question image sucessfully"))
                else:
                    error(request, _("Changed question image unsucessfully"))
            else:
                if obj.titleImage == form.cleaned_data['titleImage']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added question image sucessfully"))
                else:
                    error(request, _("Added question image unsucessfully"))
        
    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted question image sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('question image was deleted sucessfully.'),
                '%d ' + _('question images were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('question image deleted sucessfully.'),
                    '%d ' + _('question images pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)

# Klasa umożliwiająca wyświetlanie danych dotyczących stworzonego modelu GaleryImage

class GaleryImageAdmin(ExportMixin, ModelAdmin):

    action_callback = True
    list_display = ["name","sequence"]
    list_filter = ['sequence']
    search_fields = ['name','date']
    list_per_page = 20
    fieldsets = [
        (_('Image data'), {'fields': ["name","image","sequence"]}),
    ]

    def get_export_filename(self, request, queryset, file_format):
        filename = "%s.%s" % (_("GalleryImage"),
                          file_format.get_extension())
        return filename
    
    def get_actions(self, request):
        actions = super().get_actions(request)
        actions['delete_selected'][0].short_description = _("Delete Selected")
        success
        return actions
    
    def message_user(self, request, *args):
        if self.action_callback:
            success(request,args[0])
    
    def save_model(self, request, obj, form, change):
        self.action_callback = False
        if not self.action_callback:
            if change:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Changed galery image sucessfully"))
                else:
                    error(request, _("Changed galery image unsucessfully"))
            else:
                if obj.name == form.cleaned_data['name']:
                    super().save_model(request, obj, form, change)
                    success(request, _("Added galery image sucessfully"))
                else:
                    error(request, _("Added galery image unsucessfully"))
    
    def delete_model(self, request, obj):
        self.action_callback = False
        if not self.action_callback:
            super().delete_model(request, obj)
            success(request, _("Deleted galery image sucessfully"))
    
    def delete_queryset(self, request, queryset):
        self.action_callback = False
        if not self.action_callback:
            record_num = queryset.count()
            super().delete_queryset(request,queryset)
            if (((record_num == 1) or ((record_num % 10 >= 2 and record_num % 10 <= 4))) and (int(record_num/10) != 1)):
                success(request, ngettext(
                '%d ' + _('gallery image was deleted sucessfully.'),
                '%d ' + _('gallery images were deleted sucessfully.'),
                record_num,
            ) % record_num)
            else:
                success(request, ngettext(
                    '%d ' + _('gallery image deleted sucessfully.'),
                    '%d ' + _('gallery images pro were deleted sucessfully.'),
                    record_num,
                ) % record_num)

# Wyświetlanie danych zdefiniowanych w modelu w panelu administracyjnym

my_admin_site.register(User, UserAdmin)
my_admin_site.register(Company, CompanyAdmin)
my_admin_site.register(Training,TrainingAdmin)
my_admin_site.register(Question, QuestionAdmin)
my_admin_site.register(TrainingImage,TrainingImageAdmin)
my_admin_site.register(GaleryImage,GaleryImageAdmin)
my_admin_site.register(QuestionImage, QuestionImageAdmin)
my_admin_site.register(CompletedTraining,CompletedTrainingAdmin)
my_admin_site.register(Answer,AnswerAdmin)
my_admin_site.register(AuthUser)
my_admin_site.register(Group)
