from .models import *
from .serializers import *
from django.http.response import Http404
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import PageNumberPagination

# Create your views here.
def home(request):
    return render(request, 'modelszkolenie/home.html')


class CustomSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 5000

class CompanyList(ListCreateAPIView):

    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['name','address','city']
    search_fields = ['name','address','city']
    ordering_fields = ['name','address','city']
    ordering = ['name']
    pagination_class = CustomSetPagination
    
class CompanyDetail(RetrieveUpdateDestroyAPIView):

    queryset = Company.objects.all()
    serializer_class = CompanySerializer

class UserList(ListCreateAPIView):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['first_name','last_name','email','id_card','country','company__name','company__city']
    search_fields = ['first_name','last_name','email','id_card','country','company__name','company__city']
    ordering_fields = ['last_name','first_name','company__name']
    ordering = [F('last_name').asc(nulls_last=True),F('first_name').asc(nulls_last=True)]
    pagination_class = CustomSetPagination

class UserDetail(RetrieveUpdateDestroyAPIView):

    queryset = User.objects.select_related("company")
    serializer_class = UserSerializer

class TrainingList(ListCreateAPIView):

    queryset = Training.objects.prefetch_related("users","questions")
    serializer_class = TrainingSerializer
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['name','time','obligatory']
    search_fields = ['name','time','language']
    ordering_fields = ['name','time']
    ordering = ['time']
    pagination_class = CustomSetPagination

class TrainingDetail(RetrieveUpdateDestroyAPIView):

    queryset = Training.objects.prefetch_related("users","questions")
    serializer_class = TrainingSerializer

class QuestionList(ListCreateAPIView):

    queryset = Question.objects.prefetch_related("answers","image")
    serializer_class = QuestionSerializer
    filter_backends = (DjangoFilterBackend,SearchFilter)
    filter_fields = ['content','language','answers__answer']
    search_fields = ['content','language','answers__answer']
    pagination_class = CustomSetPagination

class QuestionDetail(RetrieveUpdateDestroyAPIView):

    queryset = Question.objects.prefetch_related("answers","image")
    serializer_class = QuestionSerializer

class AnswerList(ListCreateAPIView):

    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['answer']
    search_fields = ['answer']
    ordering_list=['id','answer']
    ordering= ['id']
    pagination_class = CustomSetPagination

class AnswerDetail(RetrieveUpdateDestroyAPIView):

    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class TrainingUserList(ListAPIView):

    serializer_class = CompletedTraningSerializer
    pagination_class = CustomSetPagination
    queryset = CompletedTraining.objects.prefetch_related("guest","training")
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['guest','training','completed_date']
    search_fields = ['guest','training']
    ordering_fields = ['guest__first_name','guest__last_name','training','completed_date']
    ordering = ['completed_date']
    def get_queryset(self):
        try:
            users = User.objects.get(pk=self.kwargs['pk'])
            return CompletedTraining.objects.filter(guest=users)
        except User.DoesNotExist:
            raise Http404

class QuestionsAnswerList(ListAPIView):

    serializer_class = QuestionsAnswersSetSerializer
    queryset = QuestionsAnswersSet.objects.prefetch_related("question","answer")
    filter_backends = (DjangoFilterBackend,SearchFilter,OrderingFilter)
    filter_fields = ['question','answer','status']
    search_fields = ['question__content','answer__answer','status']
    ordering_fields = ['question','answer','status']
    ordering = ['id']
    pagination_class = CustomSetPagination
    def get_queryset(self):
        try:
            asked_question = Question.objects.get(pk=self.kwargs['pk'])
            return QuestionsAnswersSet.objects.filter(question=asked_question)
        except User.DoesNotExist:
            raise Http404
                
        

    
