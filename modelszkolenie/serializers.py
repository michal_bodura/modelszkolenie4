from .forms import *
from .models import *
from rest_framework.serializers import ModelSerializer, SerializerMethodField

class CompanySerializer(ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'

class UserSerializer(ModelSerializer):

    full_name = SerializerMethodField(source='get_full_name')

    class Meta:
        model = User
        fields = ['id','first_name','last_name','full_name','email','id_card','company']
        
    def get_full_name(self, obj):
        return f'{obj.first_name} {obj.last_name}'


class AnswerSerializer(ModelSerializer):

    class Meta:
        model = Answer
        fields = ['id','answer']

class QuestionSerializer(ModelSerializer):

    class Meta:
        model = Question
        fields = '__all__'
        depth = 1

class TrainingSerializer(ModelSerializer):

    class Meta:
        model = Training
        fields = '__all__'

class CompletedTraningSerializer(ModelSerializer):

    expiration_date = SerializerMethodField()
    class Meta:
        model = CompletedTraining
        fields = ["id","guest","training","completed_date","expiration_date"]

    def get_expiration_date(self, obj):
        return obj.expiration_date

class QuestionsAnswersSetSerializer(ModelSerializer):


    class Meta:
        model = QuestionsAnswersSet
        fields = ['id','question','answer','status']


