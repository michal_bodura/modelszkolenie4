
from .admin import my_admin_site
from .models import *
from .views import *
from django.apps.registry import Apps
from django.db import models
from django.test import Client, TestCase, SimpleTestCase, TransactionTestCase
from django.urls import resolve
from datetime import datetime, date
from json import dumps
from pickle import dumps, loads


class TestsThatDependsOnPrimaryKeySequences(TransactionTestCase):
    reset_sequences = True

    def test_user_pk(self):

        user1 = User.objects.create(first_name="Michał", last_name="Bodura")
        # user1.pk is guaranteed to always be 1
        self.assertEqual(user1.pk, 1)

class TestsThatDependsOnForeignKeySequences(TransactionTestCase):

    def test_instance(self):

        c1 = Company(name="Atos", address="Testowa 54")
        c1.save()
        c2 = Company(name="IU Techology", address="Dubois 114/116")
        c2.save()
        self.assertIsInstance(c1,c2)

    def test_fk(self):

        c1 = Company(name="Atos", address="Testowa 54")
        c1.save()
        c2 = Company(name="IU Techology", address="Dubois 114/116")
        c2.save()
        u1 = User(first_name='John', last_name='Smith', email='john@example.com',company=c1)
        u1.save()
        u2 = User(first_name='Paul', last_name='Jones', email='paul@example.com')
        u2.save()

        self.assertEqual(u1.company.name,"Atos")
        
    
class UserBasicTest(TestCase):

    def setUp(self):

        self.user = User()
        self.user.first_name = "Michał"
        self.user_last_name  = "Bodura"
        self.user.save()
    
    def setUpClient(self):

        self.client = Client()

    def test_details(self):

        response = self.client.get('/admin')
        self.assertEqual(response.status_code, 301)


    def create_user(self, first_name="Michał", last_name="Bodura"):

        return User.objects.create(first_name=first_name, last_name=last_name)
    
    def test_fields(self):

        user = User()
        user.first_name = "Michał"
        user.last_name = "Badura"
        user.save()
        record = User.objects.get(pk=user.id)
        self.assertEqual(record,user)

    def test_user_creation(self):

        u = self.create_user()
        self.assertIsInstance(u, User)
    
    def test_greater_than_zero(self):

        u1 = self.setUp()
        u2 = self.setUp()
        users = [u1, u2]
        self.assertGreater(len(users), 0)
        


class TestModelDefinition(SimpleTestCase):
    
    def test_model_definition(self):
        test_apps = Apps(['modelszkolenie'])

        class TestModel(models.Model):
            class Meta:
                apps = test_apps
        
        self.assertNull()

class CompletedTrainingTestCase(TestCase):

    def setUser(self):

        self.user = User()
        self.user.first_name = "Michał"
        self.user.last_name  = "Bodura"
        self.user.save()
        return self.user

    def setSecondUser(self):

        self.user = User()
        self.user.first_name = "Piotr"
        self.user.last_name  = "Słowik"
        self.user.save()
        return self.user 
   
    
    def setTraining(self):

        self.training = Training()
        self.training.name = "Szkolenie z dokera"
        self.training.time = 14
        self.training.obligatory = True
        self.training.save()
        return self.training

    def setUp(self):

        user = self.setUser()
        training = self.setTraining()
        self.ct = CompletedTraining()

        self.ct.guest = user
        self.ct.training = training
        self.ct.completed_date  = "2021-08-17"
        self.ct.save()
        user = self.setSecondUser()
        training = self.setTraining()
        self.ct = CompletedTraining()

        self.ct.guest = user
        self.ct.training = training
        self.ct.completed_date  = "2021-08-31"
        self.ct.save()
        
        return self.ct
    
    def test_date(self):

        queryset = CompletedTraining.objects.filter(completed_date__year=2021)
        print(queryset.count())
        self.assertGreater(queryset.count(),0)
    
    def test_completedTrainingTwoPearson(self):

        queryset = CompletedTraining.objects.filter(completed_date__gte='2021-08-15')
        self.assertGreaterEqual(queryset.count(),2)
    
    def test_completedTrainingAlmostEquals(self):

        queryset = CompletedTraining.objects.filter(completed_date__gte='2021-08-15')
        self.assertAlmostEquals(queryset.count(),2)

    def test_completedTrainingNotAlmostEquals(self):

        queryset = CompletedTraining.objects.filter(completed_date__gte=datetime.today())
        self.assertNotAlmostEquals(queryset.count(),2)

    def test_completedTrainingExcluded(self):

        queryset = CompletedTraining.objects.exclude(completed_date__gte=datetime.today())
        self.assertAlmostEquals(queryset.count(),1)

class UserTestCase(TestCase):

    def test_fields(self):

        user = User(id=4,first_name="Michał", last_name="Bodura")
        user.save()
        record = User.objects.create(id=5,first_name="Michał",last_name="Bodura")
        self.assertEqual(record.first_name,user.first_name)

    def setUp(self):

        User.objects.create(first_name="Michal", last_name="Bodura")
        User.objects.create(email="piotr.nowak@iutechnology.pl")
    

    # Sprawdzenie poprawnosci modelu pod wzgledem atrybutu - poprawna name atrybutu
    def test_attributes(self):

        user1 = User.objects.get(first_name="Michal")
        user2 = User.objects.get(email="piotr.nowak@iutechnology.pl")
        queryset = [user1, user2]
        self.assertTrue(queryset,"Cos nie poszlo z atrybutami")
    

    # Sprawdzanie nazwy atrybutu - bledna name atrybutu
    def test_attributes2(self):

        user1 = User.objects.get(first_name="Michal")
        user2 = User.objects.get(email="piotr.nowak@iutechnology.pl")
        queryset = [user1, user2]
        self.assertTrue(queryset,"Cos nie poszlo z atrybutami")

    def test_ifPiotrNowakExists(self):

        User.objects.create(first_name="Piotr", last_name="Nowak")
        ex1 = User.objects.get(first_name="Piotr", last_name="Nowak")
        self.assertTrue(ex1,"Nie ma Piotra Nowaka na liscie")

    def test_ifMichalIsInList(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura")
        queryset = User.objects.filter(first_name="Michal")
        self.assertTrue(queryset,"Nie ma uzytkownika o first_nameniu Michal na liscie")

    # False
    def test_ifUsersEquals_one(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura")
        self.assertEquals(user1,user2,"Nie sa rowne")

    # True
    def test_ifUsersNotEquals(self):

        user1 = User.objects.create(first_name="Michal", last_name="Bodura",email="michal_bodura@iutechnology.pl", id_card="CDJ757557")
        user2 = User.objects.create(first_name="Michal", last_name="Bodura",email="michal_bodura@iutechnology.pl", id_card="CDJ757557")
        self.assertNotEquals(user1,user2,"Istnieje redundacja w bazie, prosze to zweryfikowac")

    # Sprawdzam typ zmiennej
    def test_type(self):

        User.objects.create(first_name="Piotr", last_name="Nowak")
        ex1 = User.objects.get(first_name="Piotr", last_name="Nowak")
        self.assertEqual(ex1.first_name + " " + ex1.last_name, 'Piotr Nowak')

    # Sprawdzam, czy dane wyświetlają się w bazie

    def test_user_data(self):

        namesUser = []
        for u in User.objects.all():
            namesUser.append(u.first_name)
        self.assertEqual(len(namesUser),2)
    
    def test_unique_user_data(self):

        namesUser = []
        for u in User.objects.all():
            if u not in namesUser:
                namesUser.append(u.first_name)
        self.assertEqual(len(namesUser),2)

    # Sprawdzam czy istnieje w bazie pracownik, który pracuje w firmie IU Technology

    def test_if_worker_in_IU_Technology(self):

        company1 = Company.objects.create(name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        self.assertEqual(user1.company.name,"IU Technology")
    
    def test_dict_object(self):

        company1 = Company.objects.create(name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        qs = User.objects.values_list('first_name', 'last_name')
        reloaded_qs = User.objects.all()
        reloaded_qs.query = loads(dumps(qs.query))
        dictObject = {'first_name': 'Testowy', 'last_name':'testowy'}
        self.assertEqual(reloaded_qs[2],dictObject)
    
    def test_JSON_object(self):

        company1 = Company.objects.create(name="IU Technology", address="Dubois 114/116")
        user1 = User.objects.create(first_name="Testowy", last_name="testowy",email="michal_bodura@iutechnology.pl", id_card="CDJ757557", company=company1)
        qs = User.objects.values_list('first_name', 'last_name')
        reloaded_qs = User.objects.all()
        reloaded_qs.query = loads(dumps(qs.query))
        dictObject = dumps({'first_name': 'Testowy', 'last_name':'testowy'})
        self.assertJSONEqual(dumps(reloaded_qs[2]),dictObject)
          
class CompanyTestCase(TestCase):

    def test_ifNotEquals(self):
        
        company1 = Company.objects.create(name="IU Technology", address="Dubois 114/116")
        company2 = Company.objects.create(name="IU Technology", address="Dubois 112")
        self.assertNotEquals(company1,company2)


class TrainingTestCase(TestCase):

    def test_ifTrainingStartsSeptember(self):
        d = date.today()

class TestUrls(TestCase):
    def test_resolution_for_user(self):
        resolver = resolve('/api/user/47')
        self.assertEqual(resolver.func.cls, UserDetail)
    
    def test_resolution_for_all_users(self):
        resolver = resolve('/api/user/')
        self.assertEqual(resolver.func.cls, UserList)
    
    def test_admin(self):
        self.assertTrue(my_admin_site.urls)

# Create your tests here.
