from .models import *
from django.utils.translation import gettext_lazy as _
from import_export.fields import Field
from import_export.resources import ModelResource

# Register your models here.
class UserResource(ModelResource):

    first_name = Field(attribute='first_name', column_name=_("first name"))
    last_name = Field(attribute='last_name', column_name=_("last name"))
    email = Field(attribute='email', column_name=_("email"))
    company = Field(attribute='company__name', column_name=_("company"))
    id_card = Field(attribute='id_card', column_name=_("id_card"))
    country = Field(attribute="country", column_name=_("country"))
    
    class Meta:
        model = User
        fields = ('id','first_name','last_name','email','company','id_card','country')
        export_order = ('id','first_name','last_name','email','company','id_card','country')
        exclude = ('company')

# Register your models here.
class CompanyResource(ModelResource):

    name = Field(attribute='name', column_name=_("name"))
    address = Field(attribute='address', column_name=_("address"))
    city = Field(attribute="city", column_name=_("city"))
    class Meta:        
        model = Company
        fields = ('id','name','address','city')
        export_order = ('id','name','address','city')

# Register your models here.
class QuestionImageResource(ModelResource):

    titleImage = Field(attribute='titleImage', column_name=_("title"))
    image = Field(attribute='image', column_name=_("image"))
    date = Field(attribute='date', column_name=_("date"))
    question = Field(attribute='question__content', column_name=_("question"))

    class Meta:
        model = QuestionImage
        fields = ('id','titleImage','image','date','question')
        export_order = ('id','titleImage','image','date','question')

# Register your models here.
class TrainingImageResource(ModelResource):
    
    titleImage = Field(attribute='titleImage', column_name=_("title"))
    image = Field(attribute='image', column_name=_("image"))
    date = Field(attribute='date', column_name=_("date"))
    training = Field(attribute='training__name', column_name=_("training"))

    class Meta:
        model = QuestionImage
        fields = ('id','titleImage','image','date','training')
        export_order = ('id','titleImage','image','date','training')


class QuestionResource(ModelResource):
    
    content = Field(attribute='content', column_name=_("content"))
    language = Field(attribute='language', column_name=_("language"))
    class Meta:
        model = Question
        fields = ('id','content','language','answers')
        export_order = ('id','content','language','answers')


class AnswerResource(ModelResource):

    answer = Field(attribute='answer', column_name=_("answer"))
    class Meta:
        model = Answer
        fields = ('id','answer')
        export_order = ('id','answer')

class TrainingResource(ModelResource):

    name = Field(attribute='name', column_name=_("name"))
    time = Field(attribute='time', column_name=_("time"))
    obligatory = Field(attribute='obligatory', column_name=_("obligatory"))
    language = Field(attribute='language', column_name=_("language"))
    class Meta:
        model = Training
        fields = ('id','name','time','obligatory','language')
        export_order = ('id','name','time','obligatory','language')


class CompletedTrainingResource(ModelResource):

    guest= Field(attribute='guest__full_name', column_name=_("Guest"))
    training = Field(attribute='training__name', column_name=_("Training"))
    completed_date = Field(attribute='completed_date', column_name=_("Completed training"))
    expiration_date = Field(attribute='expiration_date', column_name=_("Expiration date"))
    class Meta:
        model = CompletedTraining
        fields = ('id','guest','training','completed_date','expiration_date')
        export_order = ('id','guest','training','completed_date','expiration_date')

class QuestionsAnswersSetResource(ModelResource):
    
    question = Field(attribute='question', column_name=_("Question"))
    answer = Field(attribute='answer', column_name=_("Answer"))
    status = Field(attribute='status', column_name=_("Status"))
    class Meta:
        model = QuestionsAnswersSet
        fields = ("id","question","answer","status")
        export_order = ("id","question","answer","status")