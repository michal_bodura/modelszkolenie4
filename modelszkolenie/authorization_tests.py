from django.contrib.auth.models import *
from django.test import TestCase

class AuthUserTestCase(TestCase):
    
    # Testy sprawdzające czy dany użytkownik ma uprawnienia supersuera
    def test_ifSuperUser(self):
        superuser = User.objects.create_superuser(
            username='user',
            email='michal_bodura@iutechnology.pl',
            password='Monitor2020',
        )
        permission = superuser.has_perm('auth.change_user')
        self.assertTrue(permission,"Nie jestes adminem")


    
