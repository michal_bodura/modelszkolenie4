from .models import QuestionsAnswersSet,  Training, Question, Answer
from django_seed import Seed 
from random import randint


trainings = ["Szkolenie React","Szkolenie C++","Szkolenie Flusk","Szkolenie C#","Szkolenie Docker","Szkolenie Kubernetes","Advanced Python","Advanced JavaScript"]
times = [5,10,15,20,25,30,35,40,45,50,75,100,150,200]
names = ['Michał','Piotr','Radosław','Szymon','Krzysztof','Marcin','Stanisław','Szczepan']

surnames = ['Nowak','Kowalski','Piotrkowski','Darowny','Bodura','Lato','Jarosław','Nowakowski','Bielicki','Piotrkowski']
questions_contents = ["Czy jesteś w domu?","Czy siebie słyszysz?","Czy jesteś w pracy?","Czy widzałeś szefa?","Czy ogarniasz Django?","Czy nauczyłeś się Django?","Czy masz rodzeństwo?"]
user_answers = ["TAK","NIE","NIE Pamiętam"]
statuses = [True, False]

seeder = Seed.seeder()

seeder.add_entity(Training, 5, {
    'name': lambda x: trainings[randint(0, len(trainings) - 1)],
    'time': lambda x: times[randint(0, len(times) - 1)]
})

seeder.add_entity(Question, 5, {
    'content': lambda x: questions_contents[randint(0, len(questions_contents) - 1)],
})

seeder.add_entity(Answer, 5, {
    'answer': lambda x: user_answers[randint(0, len(user_answers) - 1)],
})
seeder.add_entity(QuestionsAnswersSet, 5, {
    'status': lambda x: statuses[randint(0, len(statuses) - 1)],
})


def execute():
    seeder.execute()
    print("Seeding completed")


