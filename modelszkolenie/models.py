from .validators import validate_file_extension
from datetime import timedelta
from django_countries.fields import CountryField
from django.db.models import CharField,PositiveIntegerField,FileField,BooleanField, DateTimeField, TextField, EmailField,ManyToManyField,F, Count, Model, TextChoices, ForeignKey, DateField, CASCADE
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.utils.timezone import datetime
from django.utils.translation import gettext_lazy as _
from locale import setlocale as SetLanguage, LC_ALL as ALL

SetLanguage(ALL, '')

# Definicja modelu Company w postaci klasy

class Company(Model):

    name = CharField(max_length=60, help_text=_("write company name"), verbose_name=_("name"),blank=False, null=True)
    address = CharField(max_length=120, blank=True,null=True,help_text=_("write address"), verbose_name=_("address"))
    city = CharField(max_length=180, blank=True,null=True,help_text=_("write city"), verbose_name=_("city"))

    @property
    def real_address(self):
        return f"{self.address}, {self.city}"

    class Meta: 
        
        verbose_name_plural = _("company plural")
        verbose_name =_("companyOdm")
        unique_together = ['name','address']
        db_table = "Company"
        ordering = ['name']

    def __str__(self):
        return f"{self.name}"

# Definicja modelu wyliczającego Language w postaci klasy

class Language(TextChoices):

    ENGLISH = 'EN', _('English')
    POLISH = 'PL', _('Polish')

# Definicja modelu User w postaci klasy

class User(Model):
    
    first_name = CharField(max_length=30, verbose_name=_("first name"),help_text=_("write first name"),null=True, blank=True)
    last_name = CharField(max_length=30, verbose_name=_("last name"),help_text=_("write last name"),null=True, blank=True)
    email = EmailField(help_text=_("write email"),verbose_name=_("email"),unique=True, null=True, blank=True)
    id_card = CharField(max_length=26, verbose_name=_("id_card"), help_text=_("write ID Card"),blank=False,unique=True,null=True)
    country = CountryField(blank_label=_("Choose country"),null=True, blank=True,verbose_name=_("Country"),help_text=_("Choose country"),default='PL')
    
    company = ForeignKey(
        Company, 
        related_name='companies',
        on_delete=CASCADE,
        blank=True,
        null=True,
        verbose_name=_("Company"),
        help_text=_("Choose company")
        
    )

    def __str__(self):
        return f"{self.email} - {self.id_card}"

    class Meta:
        unique_together = ['email','id_card']
        verbose_name_plural=_("Guest plural")
        verbose_name =_("GuestOdm")
        db_table = "Guest"

# Definicja modelu Answer w postaci klasy

class Answer(Model):

    answer = TextField(verbose_name=_("answer"),help_text=_("write answer"),blank=False,null=True)
    def __str__(self):
        return f"{self.answer}"
    class Meta:
        ordering=['id']
        verbose_name =_("answer")
        verbose_name_plural = _("answer plural")
        db_table = "Answer"

# Definicja modelu TrainingImage w postaci klasy

class TrainingImage(Model):
    
    titleImage = CharField(max_length=255, verbose_name=_("title"),help_text=_("write title of image"),blank=False,null=True)
    image = FileField(upload_to="images/szkolenie",help_text=_("read image"),verbose_name=_("Image"), validators=[validate_file_extension])
    date = DateTimeField(auto_now_add=True,verbose_name=_("Date"),help_text=_("write date"))

    def __str__(self):
        return f"{self.titleImage}"

    class Meta:
        verbose_name_plural = _("Gallery training Plural")
        verbose_name = _("Gallery training Odmiana")
        ordering = [F('date').desc(nulls_last=True)]
        db_table = "Training images gallery"

# Definicja modelu QuestionImage w postaci klasy

class QuestionImage(Model):
    
    titleImage = CharField(max_length=255, verbose_name=_("title"),blank=False,null=True,help_text=_("write title of image"))
    image = FileField(upload_to="images/pytanie",help_text=_("read image"),verbose_name=_("Image"), validators=[validate_file_extension])
    date = DateTimeField(auto_now_add=True, verbose_name=_("date"),help_text=_("write date"))

    def __str__(self):
        return f"{self.titleImage}"
    
    def __repr__ (self):
        return "{self.id}, {self.titleImage}"
    
    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = _("Gallery training questions plural")
        verbose_name = _("Gallery training questions odmiana")
        ordering = [F('date').desc(nulls_last=True)]
        db_table = "Question images gallery"

class GaleryImage(Model):

    name = TextField(verbose_name=_("description"),help_text=_("DescriptionHelper"), null=True,blank=False)
    image = FileField(upload_to="images/szkolenie/galeria",help_text=_("read image"),verbose_name=_("Image"), validators=[validate_file_extension])
    date = DateTimeField(auto_now_add=True, verbose_name=_("date"),help_text=_("write date"))
    sequence = PositiveIntegerField(verbose_name=_("sequence"),help_text=_("set sequence image"),default=1)
    
    def __str__(self):
        return f"{self.name}"
    
    class Meta:
        ordering = ['sequence']
        verbose_name_plural = _("Gallery image plural")
        verbose_name = _("Gallery image")
        db_table = "GaleryImage"

class Question(Model):

    content = TextField(verbose_name=_("content"),help_text=_("write question content"),blank=False,null=True)
    language = CharField(
        max_length=2,
        choices=Language.choices,
        default='PL',
        verbose_name=_("Language"),
        help_text=_("Choose language")
    )
    image = ForeignKey(
          QuestionImage, 
          related_name="question",
          on_delete=CASCADE,
          blank=True,
          null=True,
          verbose_name=_("choose image"),
          help_text=_("choose image from list"),
    )

    sequence = PositiveIntegerField(verbose_name=_("sequence"),help_text=_("set sequence question"),default=1)
    answers = ManyToManyField(Answer,through='QuestionsAnswersSet', related_name="questions_Answers", help_text=_("Choose users"))

    @property
    def numOfAnswers(self):
        return Question.objects.filter(id=self.id).annotate(num_answers=Count('answers')).values('num_answers').get().get('num_answers')

    def __str__(self):
        return f"{self.content}"

    class Meta:
        ordering = ['id']
        unique_together = ["content", "language"]
        verbose_name_plural=_("question plural")
        verbose_name=_("questionOdm")
        db_table = "Question"

# Definicja modelu Training w postaci klasy

class Training(Model):

    name = CharField(max_length=255,verbose_name=_("name"),help_text=_("write_name_training"),blank=False,null=True)
    time = PositiveIntegerField(verbose_name=_("Time"),help_text=_("Write time"),default=0)
    obligatory = BooleanField(default=False, verbose_name=_("Obligatory"),help_text=_("check obligatory"))
    
    language = CharField(
        max_length=2,
        choices=Language.choices,
        default='PL',
        verbose_name=_("Language"),
        help_text=_("Choose language")
    )

    image = ForeignKey(
          TrainingImage, 
          related_name="training",
          on_delete=CASCADE,
          blank=True,
          null=True,
          verbose_name=_("choose image"),
          help_text=_("choose image from list"),
    )

    gallery = ManyToManyField(to=GaleryImage,verbose_name=_("Gallery"), related_name="trainings_galleryImage", help_text=_("Choose images"))
    questions = ManyToManyField(to=Question,related_name="trainings_question", verbose_name=_("sequence"), help_text=_("Choose questions"))
    users = ManyToManyField(User,related_name="trainings_users",through='CompletedTraining', help_text=_("Choose users"))
 
    @property
    def numOfGuests(self):
        return Training.objects.filter(id=self.id).annotate(num_users=Count('users')).values('num_users').get().get('num_users')
        
    @property
    def numOfQuestions(self):
        return Training.objects.filter(id=self.id).annotate(num_questions=Count('questions')).values('num_questions').get().get('num_questions')

    def __str__(self):
        return f"{self.name}"
        
    class Meta:
        ordering = ['time']
        verbose_name_plural=_("Training plural")
        verbose_name=_("TrainingOdm")
        db_table = "Training"

    @property
    def time_str(self):
        if (self.time == 1):
            return f"{self.time} " + _("day")
        else:
            return f"{self.time} " + _("days")
        

# Definicja modelu CompletedTraining w postaci klasy

class CompletedTraining(Model):
    
    guest = ForeignKey(User,related_name="guests", on_delete=CASCADE,help_text=_("Choose user"), verbose_name=_("GuestList"),null=True)
    training = ForeignKey(Training, related_name="completed_trainings", on_delete=CASCADE,help_text=_("Choose training"), verbose_name=_("Training"),null=True)
    completed_date = DateField(verbose_name=_("Completed training"),help_text=_("Date of completed training"),default=datetime.now)
    
    @property
    def expiration_date(self):
        self.training.time = timedelta(days=self.training.time)
        return (self.completed_date + self.training.time) 
    
    
    def __str__(self):
        return f"Szkolenie: {self.training}, Osoba: {self.guest}"

    class Meta:
        verbose_name_plural = _("Completed training plural")
        verbose_name = _("Completed training odmiana")
        unique_together = ['guest','training']
        db_table = "Completed trainings"

# Definicja modelu QuestionsAnswersSet w postaci klasy

class QuestionsAnswersSet(Model):

    question = ForeignKey(Question, on_delete=CASCADE,help_text=_("QuestionSet"), verbose_name=_("Question"),blank=True,null=True)
    answer = ForeignKey(Answer, on_delete=CASCADE,help_text=_("AnswerSet"), verbose_name=_("Answer"),null=True,blank=True)
    status = BooleanField(default=False, verbose_name=_("Status"),help_text=_("StatusSet"))
    
    def __str__(self):
        return f"Pytanie: {self.question}, Odpowiedź: {self.answer}, Status: {self.status}"

    class Meta:
        ordering = ['id']
        verbose_name_plural = _("Q&A set plural")
        verbose_name = _("Q&A set odmiana")
        unique_together = ['question','answer','status']
        db_table = "QuestionsAnswersSet"

# Add model proxy
class TrainingQuestionProxy(Training.questions.through):

    class Meta:
        proxy = True

    def __str__(self):
        return f"Treść: {self.question.content}, Kolejność: {self.question.sequence}"
    
class TrainingGalleryProxy(Training.gallery.through):

    class Meta:
        proxy = True
    def __str__(self):
        return f"Treść: {self.galeryimage.name}, Kolejność: {self.galeryimage.sequence}"


# Add model signals for media files

@receiver(pre_delete, sender=TrainingImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)

@receiver(pre_delete, sender=GaleryImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)

@receiver(pre_delete, sender=QuestionImage)
def delete_image(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(False)
