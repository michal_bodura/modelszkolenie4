from .models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ChoiceField, Select
from django.utils.translation import gettext_lazy as _

class CustomUserCreationForm(UserCreationForm):

    country = ChoiceField(
        choices=[('pl','Polska'),('gb','Wielka Brytania')], 
        label=_("Country"),
        widget=Select(
            attrs={
                'class':'form-control m-b',
                'id':'country_sel'
            }
        )
    )

    class Meta(UserCreationForm):
        model = User
        fields = '__all__'

class CustomUserChangeForm(UserChangeForm):

    country = ChoiceField(
            choices=[('pl','Polska'),('gb','Wielka Brytania')], 
            label=_("Country"), 
            widget=Select(
            attrs={
                'class':'form-control m-b',
                'id':'country_sel'
            }
        )
    )
    
    class Meta(UserChangeForm):
        model = User
        fields = '__all__'

