from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from os import path as Path

# Walidacja rozszerzenia wczytanego pliku

def validate_file_extension(value):
    ext = Path.splitext(value.name)[1] 
    valid_extensions = ['.jpg', '.jpeg','.png','.JPG', '.JPEG','.PNG']
    if not ext.lower() in valid_extensions:
        raise ValidationError(_('Choose image only in .jpg or .png'))