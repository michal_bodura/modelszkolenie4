from django.apps import AppConfig
from django.contrib.admin import apps
from django.utils.translation import gettext_lazy as _

class ModelszkolenieConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'modelszkolenie'
    verbose_name = _('Data structure')

class MyAdminConfig(apps.AdminConfig):
    default_site = 'modelszkolenie.admin.MyAdminSite'